# README #
# Como utlizar la API de GOOGLE #

1) El JAR de la API se almacena en ApiNews/JAR/ApiNews.jar

2) Hay que agregar el jar al build path de su proyecto.

3) Se debe obtener un objeto del tipo InterfaceApi, el cual se solicita previamente de la clase ApiCommunicator.

**Ejemplo de uso:
---InterfaceApi conectorApi = ApiCommunicator.getSingletonInstance().getInterfaceA()---**

4) La interfaceApi, brinda dos metodos:

ApiFeed[] getNotices(): Retorna un arreglo de ApiFeed. Es requisito necesario agregar una URL antes de pedir las noticias

addNewUrl(String link): Agrega una url.

5) Los ApiFeed brinda dos metodos:

getTitle() retornará el título de la noticia 

getLink() retornará el link.

## Ejemplo de llamada a la API

```
#!java
public class Communicator {
	
	public Feed[] getNews() {
		
		InterfaceApi conectorApi = ApiCommunicator.getSingletonInstance().getGoogleAPI();
		//Es requisito necesario que se agregue una URL antes de pedir las noticias
		conectorApi.addNewUrl("https://newsapi.org/v1/articles?source=espn&sortBy=top&apiKey=");
		ApiFeed [] m = conectorApi.getNotices();
		Feed [] array= new Feed [m.length];		
		for (int i =0;i<m.length;i++) {
			array[i]= new FeedAdapterGoogleApi (m[i]);	
		}
		return array;
	}
	
	public void addUrl (String link) {
		InterfaceApi conectorApi = ApiCommunicator.getSingletonInstance().getGoogleAPI();
		conectorApi.addNewUrl(link);
	}
	
}

//Es una clase propia de NewsReader para modularizar las diferentes servidores de noticias
public class ApiCommunicator {
	private InterfaceApi ia;
	private static ApiCommunicator ac;
	
        //MainApiPresenter es un objeto propio de la API
	private ApiCommunicator () {
		ia = MainApiPresenter.getSingletonInstance();
	}
	
	public static ApiCommunicator getSingletonInstance() {
	    	if (ac==null) {
	    		ac = new ApiCommunicator();
	    	}
	    	return ac;
	    }
	 
	 public InterfaceApi getGoogleAPI () {
		 return ia;
	 }
	
}
```