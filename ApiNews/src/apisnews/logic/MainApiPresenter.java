package apisnews.logic;

import apisnews.model.ApiFeed;

public class MainApiPresenter implements InterfaceApi {
	
	private ParsingNews ia;
	private ManejoUrl mu;
	private static MainApiPresenter pn;
	
    private MainApiPresenter() {
    	ia = ParsingNews.getSingletonInstance();
    	mu = ManejoUrl.getSingletonInstance();
    	
    }

     public static MainApiPresenter getSingletonInstance() {
    	if (pn==null) {
    		pn = new MainApiPresenter();
    	}
    	return pn;
    }

	@Override
	public ApiFeed[] getNotices() {
		return ia.getNotices();
	}

	@Override
	public void addNewUrl(String link) {
		mu.addUrl(link);
		
	}
}
