package apisnews.logic;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import apisnews.model.ApiFeed;

class ParsingNews {
	
	private ManejoUrl mu;
	private static ParsingNews pn;
	
    private ParsingNews() {
    	mu = ManejoUrl.getSingletonInstance();
    }

     static ParsingNews getSingletonInstance() {
        if (pn == null){
        	pn = new ParsingNews();
        }
        
        return pn;
    }
	
 ApiFeed[] getNotices() {
		LinkedList<String> listaUrls = mu.getListUrl();
		LinkedList<ApiFeed> noticias = new LinkedList<ApiFeed>();
		ApiFeed[] newsArray = null;
		
		for (String s : listaUrls) {
			URL url;
			try {
				url = new URL(s);
	
				JsonReader 	rdr = Json.createReader(url.openStream());
	
				JsonObject obj = rdr.readObject();
				JsonArray results = obj.getJsonArray("articles");
				
				for (JsonObject result : results.getValuesAs(JsonObject.class)) {
					noticias.add(new ApiFeed(result.getString("title"), result.getString("url")));
				}
				
				newsArray = noticias.toArray(new ApiFeed[noticias.size()]);
			
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return newsArray;

	}
}