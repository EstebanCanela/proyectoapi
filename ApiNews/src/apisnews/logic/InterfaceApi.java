package apisnews.logic;

import apisnews.model.ApiFeed;

public interface InterfaceApi {
	
	public ApiFeed[] getNotices();
	
	public void addNewUrl (String link);
}
