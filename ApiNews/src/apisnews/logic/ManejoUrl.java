package apisnews.logic;

import java.util.LinkedList;

class ManejoUrl {
	
	private static ManejoUrl mu;
	private LinkedList<String> listaUrl;
	private String API_KEY = "909dbbe2b77046adb67db6792fd08e90";
	

	
    private ManejoUrl() {
    	listaUrl = new LinkedList<String> ();
    }

     static ManejoUrl getSingletonInstance() {
    	if (mu==null) {
    		mu = new ManejoUrl();
    	}
    	return mu;
    }
    
     void addUrl (String newUrl) {
    	String newUrlAPI = newUrl + API_KEY;
    	listaUrl.add(newUrlAPI);
    }
    
     LinkedList<String> getListUrl () {
    	return listaUrl;
    }
}
